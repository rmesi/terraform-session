resource "aws_security_group" "Terraform-VM-SG" {
  name        = "${var.ENVIRONMENT}-${var.SPOKE}-App-Server-SG"
  description = "SG for ${var.ENVIRONMENT} ${var.SPOKE} App-Server"
  vpc_id      = var.VPC_ID

  // To Allow SSH Transport
    ingress {
      from_port   = 22
      protocol    = "tcp"
      to_port     = 22
      cidr_blocks = ["0.0.0.0/0"]
    }

    // To Allow Port 80
    ingress {
      from_port   = 80
      protocol    = "tcp"
      to_port     = 80
      cidr_blocks = ["0.0.0.0/0"]
    }

    // To Allow Port 443
     ingress {
      from_port   = 443
      protocol    = "tcp"
      to_port     = 443
      cidr_blocks = ["0.0.0.0/0"]
    }

    // To allow Egress Traffic
    egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  

  tags = {
    Name = "${var.ENVIRONMENT}-${var.SPOKE}-App-Server-SG"
  }
}



resource "aws_security_group" "Terraform-ALB-SG" {
  name        = "${var.ENVIRONMENT}-${var.SPOKE}-App-ALB-SG"
  description = "SG for ${var.ENVIRONMENT} ${var.SPOKE} App Server ALB"
  vpc_id      = var.VPC_ID

  // To Allow SSH Transport
    ingress {
      from_port   = 22
      protocol    = "tcp"
      to_port     = 22
      cidr_blocks = ["0.0.0.0/0"]
    }

    // To Allow Port 80
    ingress {
      from_port   = 80
      protocol    = "tcp"
      to_port     = 80
      cidr_blocks = ["0.0.0.0/0"]
    }

    // To Allow Port 443
     ingress {
      from_port   = 443
      protocol    = "tcp"
      to_port     = 443
      cidr_blocks = ["0.0.0.0/0"]
    }

    // To allow Egress Traffic
    egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  

  tags = {
    Name = "${var.ENVIRONMENT}-${var.SPOKE}-App-ALB-SG"
  }
}