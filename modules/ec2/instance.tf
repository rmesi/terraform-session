

resource "aws_instance" "Terraform-Test-VM" {
  count = var.SERVER_COUNT
  ami           = var.AMI_ID
  instance_type = var.INSTANCE_TYPE
  key_name = var.KEY_NAME
  subnet_id = var.SERVER_SUBNET
  vpc_security_group_ids = [aws_security_group.Terraform-VM-SG.id]

  tags = {
    Name = "${var.ENVIRONMENT}-${var.SPOKE}-App-${(count.index)+1}"
  }
}




resource "aws_lb" "Terraform-Test-ALB" {
    name                       = "${var.ENVIRONMENT}-${var.SPOKE}-ALB"
    internal                   = false
    load_balancer_type         = "application"
    security_groups            = [aws_security_group.Terraform-VM-SG.id]
    subnets                    = var.SUBNET_IDS
    enable_deletion_protection = false
  
    tags = {
        Name  = "${var.ENVIRONMENT}-${var.SPOKE}-ALB"
    }
}


resource "aws_lb_target_group" "Terraform-Test-ALB-TG" {
  name     = "${var.ENVIRONMENT}-${var.SPOKE}-ALB-TG"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.VPC_ID

  health_check {
    enabled = true
    path = "/"
    protocol = "HTTP"
    port = 80
    healthy_threshold = 3
    unhealthy_threshold = 3
    timeout = 10
    interval = 120
  }
}


resource "aws_lb_listener" "Terraform-Test-ALB-Port80-Listener" {
  load_balancer_arn = aws_lb.Terraform-Test-ALB.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.Terraform-Test-ALB-TG.id
    type             = "forward"
  }
}


resource "aws_lb_target_group_attachment" "Terraform-Test-ALB-TG-Attachment" {
  target_group_arn = aws_lb_target_group.Terraform-Test-ALB-TG.arn
  target_id        = aws_instance.Terraform-Test-VM[0].id
  port             = 80
}
