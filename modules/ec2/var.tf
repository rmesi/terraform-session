

variable "AMI_ID" {}

variable "INSTANCE_TYPE" {}

variable "KEY_NAME" {}

variable "SUBNET_IDS" {}

variable "ENVIRONMENT" {}

variable "SPOKE" {}

variable "VPC_ID" {}

variable "SERVER_SUBNET" {}

variable "SERVER_COUNT" {}

variable "REGION" {}