
variable "AMI_ID" {
    type = string
    default = "ami-06a0b4e3b7eb7a300"
}

variable "INSTANCE_TYPE" {
    type = string
    default = "t2.micro"

}

variable "KEY_NAME" {
    type = string
    default = "Ansible-Tower-Key"
}

variable "SUBNET_IDS" {
    type = list
    default = ["subnet-4d1e2225", "subnet-c5289cbe", "subnet-ef3558a3"]
}

variable "ENVIRONMENT" {
    type = string
}

variable "SPOKE" {
    type = string
}

variable "VPC_ID" {
    type = string
    default = "vpc-13d3c47b"
}

variable "SERVER_SUBNET" {
    type = string
    default = "subnet-4d1e2225"
}

variable "SERVER_COUNT" {
    type = number
}


variable "REGION" {
    type = string
}
