

module "ec2"{
    source = "../../modules/ec2/"

    ENVIRONMENT = var.ENVIRONMENT
    SPOKE = var.SPOKE
    REGION = var.REGION

    AMI_ID = var.AMI_ID
    INSTANCE_TYPE = var.INSTANCE_TYPE
    KEY_NAME = var.KEY_NAME
    SUBNET_IDS = var.SUBNET_IDS
    VPC_ID = var.VPC_ID
    SERVER_SUBNET = var.SERVER_SUBNET
    SERVER_COUNT = var.SERVER_COUNT

}